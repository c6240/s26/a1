
const HTTP = require("http");

HTTP.createServer((request,response) => {

    if(request.url === "/login"){
     response.writeHead(200, {"Content-Type":"text/plain"});
     response.write("Welcome to login page.");
     response.end();
    } else {
     response.writeHead(500, {"Content-Type":"text/plain"});
     response.write("I'm sorry the page you are looking for cannot be found.");
     response.end();
    }
 
 } ).listen(3000)